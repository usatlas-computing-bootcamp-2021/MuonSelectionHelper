# The name of the package
ATLAS_SUBDIR(MuonSelectionHelper)

# External Dependencies
FIND_PACKAGE( ROOT )

# Add binary
ATLAS_ADD_LIBRARY ( MuonSelectionHelperLib MuonSelectionHelper/MuonSelectionHelper.h src/MuonSelectionHelper.cxx
		  PUBLIC_HEADERS MuonSelectionHelper
		     INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
		     LINK_LIBRARIES ${ROOT_LIBRARIES}
		     		     xAODEventInfo
				     xAODRootAccess
				     xAODMuon)
