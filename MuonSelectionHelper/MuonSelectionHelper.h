#include "xAODMuon/Muon.h"

class MuonSelectionHelper {

 public:

  // constructor
  MuonSelectionHelper() {};

  // destructor
  virtual ~MuonSelectionHelper() {};

  // checks the kinematics of the object
  bool isMuonGood(const xAOD::Muon* muon);

};
