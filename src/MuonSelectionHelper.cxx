#include "MuonSelectionHelper/MuonSelectionHelper.h"

// checks the kinematics of the object
bool MuonSelectionHelper::isMuonGood(const xAOD::Muon* muon) {

  bool passSelect(false);

  if ( muon->pt() > 10000 && fabs(muon->eta()) < 2.7 ){
    passSelect = true;
  }

  return passSelect;
}

